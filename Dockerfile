FROM openjdk:11-jdk
WORKDIR /
ADD ./target/gitlabrunner-spring-example-0.0.1-SNAPSHOT.jar gitlabrunner-spring-example-0.0.1-SNAPSHOT.jar
EXPOSE 8080
CMD ["java","-jar","gitlabrunner-spring-example-0.0.1-SNAPSHOT.jar"]
