package com.example.demo;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name = "test")
public class TestEntity {

  @Id
  @GeneratedValue(generator = "test_id_seq_generator")
  @SequenceGenerator(
      name = "test_id_seq_generator",
      sequenceName = "test_id_seq",
      allocationSize = 1)
  @Column(unique = true, nullable = false)
  private Long id;

  private String name;
}
