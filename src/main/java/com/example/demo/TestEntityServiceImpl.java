package com.example.demo;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class TestEntityServiceImpl implements TestEntityService {

  @Autowired private TestEntityRepository testEntityRepository;

  @Override
  public TestEntity createTestEntity(TestEntity testEntity) {
    return testEntityRepository.save(testEntity);
  }

  @Override
  public List<TestEntity> getTestEntity() {
    return testEntityRepository.findAll();
  }
}
