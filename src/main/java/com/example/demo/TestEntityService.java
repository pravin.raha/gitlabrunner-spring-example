package com.example.demo;

import java.util.List;

public interface TestEntityService {
  TestEntity createTestEntity(TestEntity testEntity);

  List<TestEntity> getTestEntity();
}
