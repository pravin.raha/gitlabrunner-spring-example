package com.example.demo;

import static org.junit.Assert.assertNotNull;

import java.util.List;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.util.TestPropertyValues;
import org.springframework.context.ApplicationContextInitializer;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit.jupiter.SpringExtension;

@SpringBootTest
@ExtendWith(SpringExtension.class)
@TestPropertySource(locations = "classpath:application.yml")
@ContextConfiguration(initializers = {TestEntityServiceTest.Initializer.class})
public class TestEntityServiceTest extends AbstractContainerBaseTest {

  @Autowired private TestEntityService testEntityService;

  @Test
  public void testGetEmployeePreference() {
    final List<TestEntity> testEntity = testEntityService.getTestEntity();
    assertNotNull(testEntity);
  }

  static class Initializer
      implements ApplicationContextInitializer<ConfigurableApplicationContext> {
    public void initialize(ConfigurableApplicationContext configurableApplicationContext) {
      TestPropertyValues.of(
              "spring.datasource.url=" + POSTGRES_SQL_CONTAINER.getJdbcUrl(),
              "spring.datasource.username=" + POSTGRES_SQL_CONTAINER.getUsername(),
              "spring.datasource.password=" + POSTGRES_SQL_CONTAINER.getPassword())
          .applyTo(configurableApplicationContext.getEnvironment());
    }
  }
}
