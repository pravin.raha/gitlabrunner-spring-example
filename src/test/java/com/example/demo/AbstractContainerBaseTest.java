package com.example.demo;

import org.testcontainers.containers.PostgreSQLContainer;

public class AbstractContainerBaseTest {

  public static final PostgreSQLContainer POSTGRES_SQL_CONTAINER;

  static {
    POSTGRES_SQL_CONTAINER = new PostgreSQLContainer();
    POSTGRES_SQL_CONTAINER.start();
  }
}
